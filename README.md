# Project Name

News Aggregator Web Application using Laravel and React Js

## Docker based Configuration

Copy `.env.example` and rename it to `.env`.

```bash
docker compose build --no-cache
docker compose up
```

Open a new terminal and type the following command:

```bash
docker compose exec server php artisan migrate --seed
```

This will create the desired tables and seed the database with initial data

## User Test credentials

```bash
email: innoscripta@test.com
password: 123456
```

Your app will be running at **http://localhost:3000/**.   
Click on **Load News** button to fetch news or inside server container run     
****
```bash
php artisan news:fetch
```

## Verify Tables in MySQL Container
To verify tables in the MySQL container:

```bash
docker ps  # Check MySQL container ID
docker exec -it <mysql-container-id> sh  # Replace <mysql-container-id> with the actual ID
mysql -u root -p  # Enter password (type root)
show databases;
use news;
show tables;
```


## To monitor laravel container logs

```bash
docker exec -it <container_name_or_id> sh
cd /var/www/html/storage
cat logs/laravel.log
```

## For manual Configuration:
For the client app:

```bash
cd client
npm install
npm build
 ```

For server app:
```bash
cd server
composer install
php artisan key:generate
php artisan migrate
php artisan seed
 ```
