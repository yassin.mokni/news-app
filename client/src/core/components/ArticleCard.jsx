import React from "react";
import { formatArticleDate } from "./Utils";

export function ArticleCard({ article }) {
  const imageClassName = "h-48 w-full object-cover";
  const cardClassName = "flex flex-1 flex-col justify-between bg-white p-6";

  const sanitizedData = () => ({
    __html: article.description
  })

  return (
    <div className="flex flex-col overflow-hidden rounded-lg shadow-lg">
      <div className="flex-shrink-0">
        <a
          href={article.url}
          className="text-blue-600 hover:underline"
          target="_blank"
        >
          <img
            className={imageClassName}
            src={article.url_to_image ? article.url_to_image : 'news-placeholder.png'}
            alt="article image"
          />
        </a>
      </div>
      <div className={cardClassName}>
        <div>
          <div className="mt-6">
            <div className="flex space-x-1 text-sm text-gray-500">
              <span>{formatArticleDate(article.published_at)}</span>
            </div>
          </div>
          <a href={article.url} className="mt-2 block" target="_blank">
            <p className="text-xl font-semibold text-gray-900">
              {article.title}
            </p>
            <p className="mt-3 text-base text-gray-500" dangerouslySetInnerHTML={sanitizedData()}>
            </p>
          </a>
        </div>
        <div className="mt-6 flex justify-between">
          <p className="text-sm font-medium text-gray-900">
            {article.raw_author}
          </p>
          <span className="text-gray-500">{article.apiSource}</span>
        </div>
      </div>
    </div>
  );
}
