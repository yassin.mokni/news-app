import React from "react";
import { Link, useLocation } from "react-router-dom";
import { currentUser } from "./Utils";
import { UserMenu } from "./UserMenu";

const Header = () => {
  const { pathname } = useLocation();
  const currentSlug = pathname.slice(1);
  return (
    <div className="mx-auto max-w-8xl px-2 sm:px-6 lg:px-8">
      <div className="relative flex h-16 justify-between">
        <div className="flex flex-1 items-center justify-center sm:items-stretch sm:justify-start">
          <div className="flex flex-shrink-0 items-center">
            <h1 className="font-bold text-xl text-gray-600">
              Innoscripta News
            </h1>
          </div>
          <div className="hidden sm:ml-6 sm:flex sm:space-x-8">
            <Link
              to="/"
              className={`${currentSlug
                ? "border-transparent text-gray-500 hover:border-gray-300 hover:text-gray-700"
                : "border-blue-500 text-gray-900"
                } inline-flex items-center border-b-2 px-1 pt-1 text-sm font-medium`}
            >
              Home
            </Link>
          </div>
        </div>
        <div className="absolute inset-y-0 right-0 flex items-center pr-2 sm:static sm:inset-auto sm:ml-6 sm:pr-0">
          <UserMenu currentUser={currentUser} />
        </div>
      </div>
    </div>
  );
};

export default Header;
