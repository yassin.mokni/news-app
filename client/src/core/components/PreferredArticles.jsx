import React from "react";
import { PreferredItemsList } from "./PreferredItemsList";

const PreferredArticles = ({
  loggedUser,
  preferredAuthors,
  preferredSources,
  maxDisplayedAuthors,
  maxDisplayedSources,
}) => {
  return (
    <div>
      {loggedUser && (
        <>
          {preferredAuthors.length ? <PreferredItemsList
            title="Preferred Authors"
            items={preferredAuthors}
            maxDisplayedItems={maxDisplayedAuthors}
          /> : ''}
          {preferredSources.length ? <PreferredItemsList
            title="Preferred Sources"
            items={preferredSources}
            maxDisplayedItems={maxDisplayedSources}
          /> : ''}
        </>
      )}
    </div>
  );
};

export default PreferredArticles;
