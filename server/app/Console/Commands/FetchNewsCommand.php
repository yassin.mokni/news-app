<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\Api\FetchNewsController;
use App\Services\NewsService;


class FetchNewsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'news:fetch';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetch news from given sources and store in local database';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info("Fetching news data and saving to local database");

        $newsService = app(NewsService::class);
        $fetchNewsController = new FetchNewsController($newsService);
        $fetchNewsController->fetchNews();

        $this->info("fetchign done done");
    }
}
