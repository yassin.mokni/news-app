<?php

return [

    // Source 1
    'newsapi'   => [
        'api_url'           => 'https://newsapi.org/v2/top-headlines',
        'api_key'           => env('NEWSAPI_API_KEY', ''),
        'api_parameter_name' => 'apiKey',
        'other_paremeters'  => ['country' => 'us'],
    ],

    // Source 2
    'theguardian'   => [
        'api_url'   => 'https://content.guardianapis.com/search',
        'api_key'   => env('THEGUARDIAN_API_KEY', ''),
        'api_parameter_name' => 'api-key',
    ],


    // Source 3
    'newyorktimes'   => [
        'api_url'   => 'https://api.nytimes.com/svc/search/v2/articlesearch.json',
        'api_key'   => env('NYTIMES_API_KEY', ''),
        'api_parameter_name' => 'api-key',
    ]

];
